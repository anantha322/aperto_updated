// @flow
import Utilities from '../Helper/Utilities'
const create = (baseURL: string = 'https://query.yahooapis.com/v1/public/yql') => {
    const fetchRequest = (url: string) => {
        return fetch(url).then(response => response.json())
    }

    const getWoeid = (parameter: Object | string) => {
        return fetchRequest(Utilities.createURLForWoeid(baseURL, parameter))
    }

    return {
        getWoeid
    }
}

// let's return back our create method as the default.
export default {
    create
}
