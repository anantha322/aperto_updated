import React from 'react'
import { compose } from 'redux'
import { ListItem } from 'react-native-elements'

//HOC
import twitterItemNavigate from '../HOC/twitterItemNavigate'

const TwitterTrendListItem = ({index, item, onPress}) =>{
    const rightTitle = item.tweet_volume ? item.tweet_volume.toString() : null
    return (
        <ListItem
            onPress={() => onPress(item)}
            key={index}
            title={decodeURIComponent(item.name)}
            rightTitle={rightTitle}
        />
    )
}
export default compose(
    twitterItemNavigate('TrendDetail')
)(TwitterTrendListItem)
