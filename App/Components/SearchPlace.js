import React from 'react'
import { compose } from 'redux'
import Search from 'react-native-search-box'

//HOC
import twitterSearchPlace from '../HOC/twitterSearchPlace'
import { connect } from 'react-redux'
import GeoActions from '../Redux/GeoRedux'

const SearchPlace = ({auth,getLocation}) =>{
    const onSearch = (searchString) => getLocation(searchString)
    if(auth.loggedIn){
        return <Search
            placeholder={'Enter Place or City name'}
            onSearch={onSearch}
            placeholderCollapsedMargin={70}
            searchIconCollapsedMargin={90}
        />
    }
    return null
}
const mapStateToProps = state => ({
    trends: state.trends,
    geo: state.geo,
    auth: state.auth
})

const mapDispatchToProps = dispatch => ({getLocation: name => dispatch(GeoActions.getLocation(name))})

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    twitterSearchPlace()
)(SearchPlace)
