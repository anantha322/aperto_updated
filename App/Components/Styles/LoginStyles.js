import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    error: {
        alignSelf: 'center',
        color: Colors.fire,
        fontWeight: 'bold'
    },
    socialBtn: {
        flexDirection: 'row'
    }
})
