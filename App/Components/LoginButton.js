import React from 'react'
import { View, Text } from 'react-native'
import { SocialIcon } from 'react-native-elements'
import { compose } from 'redux'
import {connect} from 'react-redux'

//HOC
import twitterLogin from '../HOC/twitterLogin'

//styles
import styles from './Styles/LoginStyles'
import LoginActions from '../Redux/LoginRedux'
import GeoActions from '../Redux/GeoRedux'

const LoginButton = ({auth, twitterLogInPress}) =>{
    const showError = function() {
        if (auth.error && auth.error.errorMessage) {
            return <Text style={styles.error}>{error.errorMessage}</Text>
        }
        return null
    }

    if (auth.loggedIn) {
        return null
    }
    return (
        <View style={styles.container}>
            <SocialIcon
                onPress={twitterLogInPress}
                title="Login With Twitter"
                button
                type="twitter"
                style={styles.socialBtn}
            />
            {showError()}
        </View>
    )

}
const mapStateToProps = state =>({
    auth: state.auth,
    startup: state.startup,
    geo: state.geo
})
const mapDispatchToProps = dispatch => ({
    twitterLogInPress: () => dispatch(LoginActions.login()),
    getGeoPoint: () => dispatch(GeoActions.getGeoPoint())
})

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    twitterLogin()
)(LoginButton)
