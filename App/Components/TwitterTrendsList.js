import React from 'react'
import { compose } from 'redux'
import { FlatList, Text } from 'react-native'
//HOC
import twitterLoadTrends from '../HOC/twitterLoadTrends'

//Components
import TwitterTrendListItem from './TwitterTrendListItem'

//styles
import styles from './Styles/TwitterTrendsListStyles'
import TrendsActions from '../Redux/TrendsRedux'
import { connect } from 'react-redux'

const TwitterTrendsList = ({trends, auth, navigation}) =>{
    const keyExtractor = item => item.toString()

    // Render each list trends
    const renderListItem = (item, index) => {
        return <TwitterTrendListItem
            navigation={navigation}
            item={item}
            index={index}
        />
    }
    // Render all trends
    const renderItems = (items) =>{
        return (
            <FlatList
                data={items.allIds}
                renderItem={({ item }) => renderListItem(items.byId[item], item)}
                keyExtractor={keyExtractor}
            />
        )
    }
    // Render empty data
    const renderEmptyData =() =>{
        if(trends.error.errorCode){
            return <Text style={styles.noDataText}>{trends.error.errorMessage}</Text>
        }
        return null
    }

    const currentTrends = trends.trends
    if (currentTrends
      && currentTrends.allIds.length > 0
      && auth.loggedIn) {
        return renderItems(currentTrends)
    } else if(!auth.loggedIn){
        return null
    }
    return renderEmptyData()
}
const mapStateToProps = state => ({
    trends: state.trends,
    auth: state.auth,
    startup: state.startup,
    geo: state.geo
})

const mapDispatchToProps = dispatch => ({
    getTrends: () => dispatch(TrendsActions.getTrends())
})

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    twitterLoadTrends()
)(TwitterTrendsList)
