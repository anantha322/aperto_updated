import React, { PureComponent } from 'react'
import { WebView } from 'react-native'
import { connect } from 'react-redux'

class TrendDetailsScreen extends PureComponent {
    render() {
        const { url } = this.props.data
        return <WebView source={{ uri: url }} />
    }
}

const mapStateToProps = (state, props) => ({
    ...props.navigation.state.params
})

export default connect(
    mapStateToProps,
    null
)(TrendDetailsScreen)
