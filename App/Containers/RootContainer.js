import React, { PureComponent } from 'react'
import { View, StatusBar, YellowBox } from 'react-native'
import { connect } from 'react-redux'
import { compose } from 'redux'

import Navigation from '../Navigation/AppNavigation'
import StartupActions from '../Redux/StartupRedux'
import LoginActions from '../Redux/LoginRedux'
import initialise from '../HOC/initialise'
// Styles
import styles from './Styles/RootContainerStyles'

const RootContainer = () =>{
    return (<View style={styles.applicationView}>
        <StatusBar barStyle="dark-content" />
        <Navigation /></View>
    )
}

const mapStateToProps = state => ({
    start: state.startup
})
const mapDispatchToProps = dispatch => ({
    startup: () => dispatch(StartupActions.startup()),
    twitterConfig: () => dispatch(LoginActions.twitterConfig())
})

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    initialise()
)(RootContainer)
