import '../Config'
import React from 'react'
import { Provider } from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../Redux'

// create our store
const store = createStore()

export default () => {
    return (
        <Provider store={store}>
            <RootContainer />
        </Provider>
    )
}
