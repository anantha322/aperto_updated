import React, { PureComponent } from 'react'
import {View} from 'react-native'

//Components
import {
    LoginButton,
    SearchPlace,
    TwitterTrendsList
} from '../Components'
//styles
import styles from './Styles/LaunchScreenStyles'

class LaunchScreen extends PureComponent{
    render(){
        const {navigation} = this.props
        return <View style={styles.container}>
            <LoginButton />
            <SearchPlace />
            <TwitterTrendsList navigation={navigation} />
        </View>
    }
}
export default LaunchScreen
