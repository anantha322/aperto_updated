import { Text } from 'react-native'
import AppConfig from './AppConfig'

Text.defaultProps.allowFontScaling = AppConfig.allowTextFontScaling
