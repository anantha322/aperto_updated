// @flow

type BaseTrends = {
    name: string,
    promoted_content: string,
    query: string,
    tweet_volume: number,
    url: string
}
type Coordinates = {
    longitude: number,
    latitude: number
}
type errorTypes = {
    errorCode: string,
    errorMessage: string
}

type User = {
    email: string,
    authTokenSecret: string,
    userID: string,
    authToken: string,
    userName: string
}

export type TrendsFromList = BaseTrends
export type UserCoordinates = Coordinates
export type ErrorTypes = errorTypes
export type UserTypes = User
