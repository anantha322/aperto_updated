// @flow

import type { TrendsFromList } from '../types'

export const normalizeTrendsState = (trends: Array<TrendsFromList>) => {
    let byId = {}
    trends.forEach((trend, index) => {
        byId[index] = trend
    })
    return {
        byId,
        allIds: Object.keys(byId)
    }
}

// export const normalizeTrendsState = (venues: Array<TrendsFromList>) => {
//     return venues.reduce((previous, current, index) => {
//         return {
//             byId: {...previous.byId, [index]: current},
//             allIds: [...previous.allIds, index]
//         }}, {byId: {}, allIds: []})
// }
