// @flow
import { put, select, call, take } from 'redux-saga/effects'
import TrendsAction from '../Redux/TrendsRedux'
import { NativeModules, Platform } from 'react-native'
import { GeoTypes } from '../Redux/GeoRedux'

const { RNTwitterBridge } = NativeModules
const twitterEndPoint = 'https://api.twitter.com/1.1/trends/place.json'

const getGeoId = state => state.geo.geoId

function* waitFor() : Generator<*,*,*> {
    const geoId = yield select(getGeoId)
    while (true) {
        yield take(GeoTypes.SAVE_GEO_ID)
        const newGeoId = yield select(getGeoId)
        if (newGeoId !== geoId) return
    }
}

function* listenToGeoIdChanges(): Generator<*,*,*> {
    yield call(waitFor)
    yield getTrends()
}

export function* getTrends() : Generator<*,*,*> {
    try {
        const geoId = yield select(getGeoId)
        let params
        if(Platform.OS ==='ios') {
            params = {
                id: geoId.toString()
            }
        } else{
            params = geoId.toString()
        }
        const TrendsData = yield RNTwitterBridge.getTrends(params)
        const data = (typeof TrendsData === 'string') ? JSON.parse(TrendsData) : TrendsData
        const trends = data.data ? data.data[0].trends : []
        yield put(TrendsAction.saveTrends(trends))
    } catch (e) {
        // TODO : Handle error here
        const error ={
            errorCode:'ERROR_GET_TRENDS',
            errorMessage:'No Trends available for this location'
        }
        yield put(TrendsAction.errorGetTrends(error))
    }
    yield listenToGeoIdChanges()

}
