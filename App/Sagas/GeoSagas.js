import Utilities from '../Helper/Utilities'
import { put, call } from 'redux-saga/effects'
import GeoActions from '../Redux/GeoRedux'
import Api from '../Services/Api'

const api = Api.create()

export function* getWoeid(coordinates) {
    try {
        const result = yield call(api.getWoeid, coordinates)
        let { place } = result.query.results
        if (Array.isArray(place)) {
            place = place[0]
        }
        const { woeid } = place
        yield put(GeoActions.saveGeoId(woeid))
    } catch (e) {
        // TODO: Handle error here
    }
}

export function* getGeoPoint() {
    try {
        const position = yield Utilities.watchPosition()
        if (position.coords) {
            const coordinates = {
                longitude: position.coords.latitude,
                latitude: position.coords.longitude
            }
            yield put(GeoActions.saveGeoPoint(coordinates))
            yield getWoeid(coordinates)
        } else {
            yield put(GeoActions.errorGeoPoint(position.error))
        }
    } catch (e) {
        // TODO: Handle error here
    }
}

export function* getLocation(action) {
    const { location } = action
    try {
        yield getWoeid(location)
    } catch (e) {
        // TODO: Handle error here
    }
}

