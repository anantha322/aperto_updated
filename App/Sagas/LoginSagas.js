import { put } from 'redux-saga/effects'
import LoginAction from '../Redux/LoginRedux'
import { NativeModules } from 'react-native'
const { RNTwitterBridge } = NativeModules

// TODO : move twitter setup config to env file
const Constants = {
    TWITTER_COMSUMER_KEY: 'pDWP3UHVpFKKe4QjZOueKAHvA',
    TWITTER_CONSUMER_SECRET: '0OwBV4z1rNSCgmxFahTlhxwlLpBCh9qDfYpT855VfOEomyMRCz'
}

export function* twitterConfig(action) {
    try {
        yield RNTwitterBridge.init(
            Constants.TWITTER_COMSUMER_KEY,
            Constants.TWITTER_CONSUMER_SECRET
        )
    } catch (e) {
        // TODO: Handle error here
    }
}
export function* login(action) {
    try {
        const loginData = yield RNTwitterBridge.logIn()
        yield put(LoginAction.loginSuccess(loginData))
    } catch (e) {
        // TODO: Handle error here
        const error = {
            errorCode: 'LOGIN_ERROR',
            errorMessage: 'LoginButton Error!!!'
        }
        yield put(LoginAction.loginError(error))
    }
}
