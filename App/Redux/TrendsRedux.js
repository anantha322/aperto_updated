// @flow

import { createActions, createReducer } from 'reduxsauce'
import { normalizeTrendsState } from '../Transforms/TrendsList'
import type { TrendsFromList, ErrorTypes } from '../Types'

const { Types, Creators } = createActions({
    getTrends: null,
    saveTrends: ['trends'],
    errorGetTrends:['error']
})

export const TrendsTypes = Types
export default Creators

type State = {
    trends: {
        allIds: Array<string>,
        byId: Object
    },
    error:ErrorTypes
}

export const INITIAL_STATE: State = {
    trends: {
        allIds: [],
        byId: {}
    },
    error: {
        errorCode: '',
        errorMessage: ''
    }
}

export const saveTrends = (state: State, { trends }: { trends: Array<TrendsFromList> }) => ({
    ...state,
    trends: normalizeTrendsState(trends),
    error: INITIAL_STATE.error
})
export const errorGetTrends = (state: State, { error }: { error: ErrorTypes }) =>
    ({
        ...INITIAL_STATE,
        error: error
    }: State)


export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_TRENDS]: null,
    [Types.SAVE_TRENDS]: saveTrends,
    [Types.ERROR_GET_TRENDS]: errorGetTrends
})
