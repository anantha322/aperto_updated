import { createActions, createReducer } from 'reduxsauce'
import type { UserTypes, ErrorTypes } from '../Types'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    twitterConfig: null,
    login: null,
    loginSuccess: ['user'],
    loginError: ['error']
})

export const LoginTypes = Types
export default Creators

export type State = {
    user: UserTypes,
    loggedIn: boolean,
    error: ErrorTypes
}

export const INITIAL_STATE: State = {
    user: {},
    loggedIn: false,
    error: {
        errorCode: '',
        errorMessage: ''
    }
}

export const loginSuccess = (state: State, { user }: { user: UserTypes }) =>
    ({
        ...state,
        user: user,
        loggedIn: true,
        error: {}
    }: State)
export const loginError = (state: State, { error }: { error: ErrorTypes }) =>
    ({
        ...state,
        loggedIn: false,
        user: {},
        error: error
    }: State)

export const reducer = createReducer(INITIAL_STATE, {
    [Types.TWITTER_CONFIG]: null,
    [Types.LOGIN]: null,
    [Types.LOGIN_SUCCESS]: loginSuccess,
    [Types.LOGIN_ERROR]: loginError
})
