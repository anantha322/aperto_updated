// @flow
import React, { Component } from 'react'

//Type checking
import type {ComponentType} from 'react'
import DebugConfig from '../Config/DebugConfig'
import { YellowBox } from 'react-native'
import ReduxPersist from '../Config/ReduxPersist'
type wrappedComponentType = ComponentType<*>
type initialiseType = () => wrappedComponentType => wrappedComponentType

const initialise : initialiseType = () => WrappedComponent => {
    class Initialise extends Component<*> {
        constructor(props) {
            super(props)
            YellowBox.ignoreWarnings(DebugConfig.warningReactNative)
        }
        componentDidMount() {
            if (!ReduxPersist.active) {
                this.props.startup()
            }
        }
        componentDidUpdate(prevProps) {
            if (this.props.start.active !== prevProps.start.active) {
                // Initialize twitter
                this.props.twitterConfig()
            }
        }

        render() {
            return <WrappedComponent
                {...this.state}
                {...this.props} />
        }
    }

    return Initialise
}
export default initialise
