// @flow
import React, { Component } from 'react'

//Type checking
import type {ComponentType} from 'react'
type wrappedComponentType = ComponentType<*>
type twitterLoginType = () => wrappedComponentType => wrappedComponentType

const twitterLogin:twitterLoginType = () => WrappedComponent => {
    class TwitterLogin extends Component<*> {
        isFirstTimeLoggedIn = true
        checkIfNotLoggedIn = () =>{
            const {startup, auth, twitterLogInPress} = this.props
            if (startup.active && !auth.loggedIn && this.isFirstTimeLoggedIn) {
                twitterLogInPress()
                this.isFirstTimeLoggedIn = false
            }
        }
        getGeoPointIfLoggedIn = (prevProps) =>{
            const {startup, auth, getGeoPoint} = this.props
            if (startup.active && auth.loggedIn &&
              (prevProps.auth.loggedIn !== auth.loggedIn)) {
                getGeoPoint()
            }
        }

        componentDidMount(){
            this.checkIfNotLoggedIn()
        }
        componentDidUpdate(prevProps) {
            this.checkIfNotLoggedIn()
            this.getGeoPointIfLoggedIn(prevProps)
        }

        render() {
            return <WrappedComponent
                {...this.state}
                {...this.props} />
        }
    }

    return TwitterLogin
}
export default twitterLogin
