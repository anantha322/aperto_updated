// @flow
import React, { Component } from 'react'

//Type checking
import type {ComponentType} from 'react'
import type { TrendsFromList } from '../Types'
type wrappedComponentType = ComponentType<*>
type twitterSearchPlaceType = () => wrappedComponentType => wrappedComponentType
type State = {
  trends: Array<TrendsFromList> | null
}

const twitterSearchPlace: twitterSearchPlaceType = () => WrappedComponent => {
    class TwitterSearchPlace extends Component<*, State> {
        state = { trends: null }
        dataDownloaded = false
        componentDidMount() {
            const {auth,getTrends} = this.props
            if (!this.dataDownloaded && auth.loggedIn) {
                getTrends()
            }
        }
        render() {
            return <WrappedComponent {...this.state} {...this.props} />
        }
    }
    return TwitterSearchPlace
}
export default twitterSearchPlace
