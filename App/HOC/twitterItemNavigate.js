// @flow
import React, { Component } from 'react'

//Type checking
import type {ComponentType} from 'react'
type wrappedComponentType = ComponentType<*>
type twitterItemNavigateType = (screenName: string) => wrappedComponentType => wrappedComponentType

const twitterItemNavigate:twitterItemNavigateType = screenName => WrappedComponent => {
    class TwitterItemNavigate extends Component<*> {
        onPress = (itemDetail) =>{
            this.props.navigation.navigate(screenName, { data: itemDetail })
        }
        render() {
            return <WrappedComponent
                onPress={this.onPress}
                {...this.state}
                {...this.props} />
        }
    }
    return TwitterItemNavigate
}
export default twitterItemNavigate
