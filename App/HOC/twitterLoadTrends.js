// @flow
import React, { Component } from 'react'

//Type checking
import type { TrendsFromList } from '../Types'
import type {ComponentType} from 'react'
type wrappedComponentType = ComponentType<*>
type twitterLoadTrendsType = () => wrappedComponentType => wrappedComponentType
type State = {
  trends: Array<TrendsFromList> | null
}
const twitterLoadTrends: twitterLoadTrendsType = () => WrappedComponent => {
    class TwitterLoadTrends extends Component<*, State> {
        state = {trends: null}
        dataDownloaded = false
        componentDidMount() {
            const {auth, getTrends } = this.props
            if (!this.dataDownloaded
              && auth.loggedIn) {
                getTrends()
            }
        }
        componentDidUpdate(prevProps) {
            const {startup, trends, auth, getTrends, geo } = this.props
            if (startup.active !== prevProps.startup.active
              && auth.loggedIn) {
                getTrends()
            }
            if(startup.active &&
              (prevProps.geo.geoPoint !== geo.geoPoint )){
                getTrends()
            }
            if (trends.trends !== prevProps.trends.trends) {
                this.setState({ trends: trends.trends })
                this.dataDownloaded = true
            }
        }

        render() {
            return <WrappedComponent {...this.state} {...this.props} />
        }
    }

    return TwitterLoadTrends
}

export default twitterLoadTrends
