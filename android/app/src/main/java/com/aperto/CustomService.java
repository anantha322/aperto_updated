package com.aperto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CustomService {
    @GET("/1.1/trends/place.json")
    Call<ArrayList> getPlace(@Query("id") String id);
}
