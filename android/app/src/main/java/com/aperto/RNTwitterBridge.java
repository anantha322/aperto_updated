package com.aperto;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import java.util.ArrayList;
import retrofit2.Call;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.google.gson.Gson;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;


public class RNTwitterBridge extends ReactContextBaseJavaModule implements ActivityEventListener {
    private TwitterAuthClient twitterAuthClient;
    private final ReactApplicationContext reactContext;

    public RNTwitterBridge(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNTwitterBridge";
    }

    @ReactMethod
    public void init(String CONSUMER_KEY, String CONSUMER_SECRET) {
        TwitterConfig config = new TwitterConfig.Builder(this.reactContext)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(CONSUMER_KEY, CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }
    @Override
    public void onNewIntent(Intent intent) {
    }

    @Override
    public void onActivityResult(Activity currentActivity, int requestCode, int resultCode, Intent data) {
        if (twitterAuthClient != null && twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    @ReactMethod
    public void logIn(final Promise promise) {
        twitterAuthClient = new TwitterAuthClient();
        twitterAuthClient.authorize(getCurrentActivity(), new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = result.data;
                TwitterAuthToken twitterAuthToken = session.getAuthToken();
                final WritableMap map = Arguments.createMap();
                map.putString("authToken", twitterAuthToken.token);
                map.putString("authTokenSecret", twitterAuthToken.secret);
                map.putString("name", session.getUserName());
                map.putString("userID", Long.toString(session.getUserId()));
                map.putString("userName", session.getUserName());
                twitterAuthClient.requestEmail(session, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        map.putString("email", result.data);
                        promise.resolve(map);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        map.putString("email", "COULD_NOT_FETCH");
                        promise.reject("COULD_NOT_FETCH", map.toString());
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                promise.reject("USER_CANCELLED", exception.getMessage(), exception);
            }
        });
    }

    @ReactMethod
    public void getTrends(String id, final Promise promise) {
        final TwitterSession activeSession = TwitterCore.getInstance()
                .getSessionManager().getActiveSession();
        MyTwitterApiClient twitterApiClient = new MyTwitterApiClient(activeSession);
        CustomService placeService = twitterApiClient.getCustomService();
        Call<ArrayList> call = placeService.getPlace(id);
        call.enqueue(new Callback<ArrayList>() {
            @Override
            public void success(Result<ArrayList> result) {
                Gson gson = new Gson();
                String json =  gson.toJson(result);
                promise.resolve(json);
            }
            @Override
            public void failure(TwitterException exception) {
                promise.reject("error", "error");
            }
        });

    }

}
