//
//  RNTwitterBridge.m
//  Aperto
//
//  Created by Anantha Bhat on 13/07/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <TwitterKit/TwitterKit.h>
#import <TwitterCore/TWTRCoreOAuthSigning.h>
#import <React/RCTConvert.h>
#import <React/RCTUtils.h>
#import "RNTwitterBridge.h"

@implementation RNTwitterBridge
  
- (dispatch_queue_t)methodQueue
  {
    return dispatch_get_main_queue();
  }
  RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(init: (NSString *)consumerKey consumerSecret:(NSString *)consumerSecret resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  [[Twitter sharedInstance] startWithConsumerKey:consumerKey consumerSecret:consumerSecret];
}
RCT_EXPORT_METHOD(logIn: (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession * _Nullable session, NSError * _Nullable error) {
    if (error) {
      reject(@"Error", @"Twitter signin error", error);
    } else {
      TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
      NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                       URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                     error:nil];
      [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *jsonError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        NSString *email = json[@"email"] ?: @"";
        NSDictionary *body = @{@"authToken": session.authToken,
                               @"authTokenSecret": session.authTokenSecret,
                               @"userID":session.userID,
                               @"email": email,
                               @"userName":session.userName};
        resolve(body);
      }];
    }
  }];
}

RCT_EXPORT_METHOD(logOut)
{
  TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
  NSString *userID = store.session.userID;
  [store logOutUserID:userID];
}

  RCT_EXPORT_METHOD(getTrends: (NSDictionary *) params
                    resolve :(RCTPromiseResolveBlock)resolve
                    rejecter:(RCTPromiseRejectBlock)reject)
  {
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    NSError *clientError;
    NSString * twitterEndPoint=@"https://api.twitter.com/1.1/trends/place.json";
    NSString * method = @"GET";

    NSURLRequest *request = [client URLRequestWithMethod:method URL:twitterEndPoint parameters:params error:&clientError];
    if (request) {
      [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data) {
          // handle the response data e.g.
          NSError *jsonError;
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
          NSDictionary *body = @{@"data": json};
          NSLog(@"success: %@", json);
          resolve(body);
        }
        else {
          NSLog(@"Error: %@", connectionError);
          reject(@"Error", @"Sorry, that page does not exist.", connectionError);
        }
      }];
    }
    else {
      NSLog(@"Error: %@", clientError);
      reject(@"Error", @"Twitter signin error", clientError);
    }
    
  }
  
  @end
